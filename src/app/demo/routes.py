# -*- coding: utf-8 -*-
import logging
import os
from _locale import gettext
from flask import render_template, flash, redirect, url_for,\
    request, current_app, jsonify, send_file
from flask_login import login_required, current_user
from flask_babel import lazy_gettext as _
from flask_principal import Permission, RoleNeed
from collections import OrderedDict
from app import db
from app.models import User, Role, Medicion
from app.demo.forms import UserForm, DatesForm
from app.email import random_password
from app.demo import demo
from app.demo.NeuralNetwork import NeuralNetwork
import numpy as np
from flask.json import JSONEncoder
from datetime import datetime


# Definicion de permisos
admin_permission = Permission(RoleNeed(Role.Administrador))
adviser_permission = Permission(RoleNeed(Role.Asesor))
client_permission = Permission(RoleNeed(Role.Cliente))
admin_or_adviser_permission = Permission(RoleNeed(Role.Administrador), RoleNeed(Role.Asesor))
all_permission = Permission(RoleNeed(Role.Administrador), RoleNeed(Role.Asesor), RoleNeed(Role.Cliente))


@demo.route('/')
@login_required
def index():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('.temperatura'))
    else:
        return redirect(url_for('auth.login'))


@demo.route('/users')
@admin_permission.require(http_exception=403)
@login_required
def users():
    User.actions_prop = property(lambda self: OrderedDict([
        ('edit', '.user_edit'),
        ('delete', '.user_delete')
    ]))

    pagination = User.query.order_by(User.id.asc()).paginate(
        per_page=current_app.config['ITEMS_PER_PAGE'],
        error_out=False
    )
    return render_template('users.html', pagination=pagination, roles=Role.as_dict())


@demo.route('/user/', methods=['GET', 'POST'], defaults={'id': None})
@demo.route('/user/<int:id>', methods=['GET', 'POST'])
@admin_permission.require(http_exception=403)
@login_required
def user_edit(id=None):
    form = UserForm()
    form.role.choices = Role.choices()

    if form.validate_on_submit():
        edited_password = False
        password_str = form.password.data

        if id:
            obj = User.query.get_or_404(id)
            if form.password.data != "" and not obj.verify_password(form.password.data):
                edited_password = True
            elif form.random_password.data is True:
                password_str = random_password()
                obj.password = password_str
                edited_password = True
        else:
            obj = User()
            if form.password.data == "" and form.random_password.data is False:
                form.password.errors = True
                return render_template('user_edit.html', form=form, admin_view=admin_view(is_active='users'))
            elif form.random_password.data is True:
                password_str = random_password()
                obj.password = password_str

        try:
            form.to_model(obj)
            db.session.add(obj)
            db.session.commit()
        except Exception as e:
            print(e)

        msg = _('User edited') if id else _('User created')
        flash(msg)

        return redirect(url_for('.users'))
    else:
        if id:
            obj = User.query.get_or_404(id)
            form.from_model(obj)
    return render_template('user_edit.html', form=form)


@demo.route('/temperatura', methods=['GET', 'POST'])
@all_permission.require(http_exception=403)
@login_required
def temperatura():
    form = DatesForm()
    mediciones = Medicion.query
    if request.method == 'GET':
        mediciones = mediciones.all()
        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones],
                      "temperatura": [m.temperatura for m in mediciones]}
        return render_template('temperatura.html', mediciones=mediciones, form=form,
                               menu_option=url_for('.temperatura'))
    elif request.method == 'POST':
        inicio = request.form.get('inicio', '')
        termino = request.form.get('termino', '')

        try:
            form.inicio.data = inicio
            inicio = datetime.strptime(inicio, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        try:
            form.termino.data = termino
            termino = datetime.strptime(termino, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        if inicio:
            mediciones = mediciones.filter(Medicion.fecha >= inicio)
        if termino:
            mediciones = mediciones.filter(Medicion.fecha <= termino)

        mediciones = mediciones.all()

        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones],
                      "temperatura": [m.temperatura for m in mediciones]}
        return render_template('temperatura.html', mediciones=mediciones, form=form, menu_option=url_for('.temperatura'))


@demo.route('/humedad', methods=['GET', 'POST'])
@all_permission.require(http_exception=403)
@login_required
def humedad():
    form = DatesForm()
    mediciones = Medicion.query
    if request.method == 'GET':
        mediciones = mediciones.all()
        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones],
                      "humedad": [m.humedad for m in mediciones]}
        return render_template('humedad.html', mediciones=mediciones, form=form, menu_option=url_for('.humedad'))
    elif request.method == 'POST':
        inicio = request.form.get('inicio', '')
        termino = request.form.get('termino', '')

        try:
            form.inicio.data = inicio
            inicio = datetime.strptime(inicio, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        try:
            form.termino.data = termino
            termino = datetime.strptime(termino, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        if inicio:
            mediciones = mediciones.filter(Medicion.fecha >= inicio)
        if termino:
            mediciones = mediciones.filter(Medicion.fecha <= termino)

        mediciones = mediciones.all()

        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones],
                      "humedad": [m.humedad for m in mediciones]}
        return render_template('humedad.html', mediciones=mediciones, form=form, menu_option=url_for('.humedad'))


@demo.route('/evapotranspiracion', methods=['GET', 'POST'])
@all_permission.require(http_exception=403)
@login_required
def evapotranspiracion():
    form = DatesForm()
    mediciones = Medicion.query
    if request.method == 'GET':
        mediciones = mediciones.all()
        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones], "et0": [m.et0 for m in mediciones]}
        return render_template('evapotranspiracion.html', mediciones=mediciones, form=form,
                               menu_option=url_for('.evapotranspiracion'))
    elif request.method == 'POST':
        inicio = request.form.get('inicio', '')
        termino = request.form.get('termino', '')

        try:
            form.inicio.data = inicio
            inicio = datetime.strptime(inicio, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        try:
            form.termino.data = termino
            termino = datetime.strptime(termino, "%d-%m-%Y")
        except Exception as e:
            print(e)
            logging.exception(e)

        if inicio:
            mediciones = mediciones.filter(Medicion.fecha >= inicio)
        if termino:
            mediciones = mediciones.filter(Medicion.fecha <= termino)

        mediciones = mediciones.all()

        mediciones = {"fechas": [m.fecha.strftime("%d-%m-%Y") for m in mediciones], "et0": [m.et0 for m in mediciones]}
        return render_template('evapotranspiracion.html', mediciones=mediciones, form=form,
                               menu_option=url_for('.evapotranspiracion'))


@demo.route('/pronostico')
@all_permission.require(http_exception=403)
@login_required
def pronostico():
    from config import Config
    from datetime import timedelta

    mediciones = Medicion.query.order_by(Medicion.fecha.desc()).limit(4).all()
    mediciones.reverse()

    nn = NeuralNetwork()

    num_input = 4
    forecast_days = 5
    max_value = np.load(os.path.join(Config.DEMO_ROOT, "max_value.npy"))
    horizon_forecast = np.array([m.temperatura/max_value for m in mediciones])
    dates = []
    last_day = mediciones[-1].fecha

    # Estrategia recursiva

    for i in range(forecast_days):
        nn.input = horizon_forecast
        nn.feedforward()
        horizon_forecast = np.append(horizon_forecast[1:], nn.output)
        last_day += timedelta(days=1)
        dates.append(last_day)

    mediciones = {"fechas": [m.strftime("%d-%m-%Y") for m in dates],
                  "temperatura": [round(h, 2) for h in horizon_forecast*max_value]}
    return render_template('pronostico.html', mediciones=mediciones, menu_option=url_for('.pronostico'))


@demo.route('/profile')
@admin_permission.require(http_exception=403)
@login_required
def profile():
    return render_template('profile.html')


@demo.route('/download_mediciones')
@all_permission.require(http_exception=403)
@login_required
def download_mediciones():
    try:
        from openpyxl import Workbook
        from io import BytesIO

        wb = Workbook()
        ws = wb.active

        row = [
            _("ID"),
            _("Temperatura"),
            _("Humedad"),
            _("Evapotranspiracion"),
            _("Fecha"),
        ]

        try:
            row = [str(field) for field in row]
        except Exception as e:
            print(e)
            logging.exception(e)

        ws.append(row)
        mediciones = Medicion.query.order_by(Medicion.fecha.asc()).all()
        for medicion in mediciones:
            row = [
                medicion.id,
                medicion.temperatura,
                medicion.humedad,
                medicion.et0,
                medicion.fecha.strftime('%d-%m-%Y'),
            ]
            ws.append(row)

        out = BytesIO()
        wb.save(out)
        out.seek(0)

        response = send_file(out,
                             attachment_filename="Mediciones.xlsx",
                             as_attachment=True)
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response
    except Exception as e:
        logging.exception(e)
        flash(_('Error'))
        return redirect(request.referrer)


class CommentaryEncoder(JSONEncoder):
    def default(self, commentary):
        try:
            return {'id': commentary.id, 'body': commentary.body}
        except Exception as e:
            print(e)
            return None

    @classmethod
    def custom_encode(cls, commentary):
        try:
            return {'id': commentary.id,
                    'body': commentary.body,
                    'local_created_at': commentary.local_created_at.strftime('%Y-%m-%d %H:%M:%S'),
                    'format_created_at': commentary.local_created_at.strftime('%d %b %Y'),
                    'created_by': commentary.created_by.full_name,
                    'attached_file': url_for('.attached_file',
                                             id=commentary.attached_files.first().id) if commentary.attached_files.first() else None}
        except Exception as e:
            print(e)
            return None


@demo.errorhandler(403)
def redirect_page(e):
    return redirect(url_for('.index'))
