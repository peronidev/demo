#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app
from flask_script import Manager
from app import db
from flask_migrate import MigrateCommand
from app.models import User, Role, Medicion
from config import Config
import logging


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def drop_all(reflect=False):
    """
    Custom drop_all
    """
    if reflect:
        db.reflect()
    db.drop_all()


@manager.command
def create_all():
    """
    Create all from sqlalchemy db
    """
    db.create_all()


@manager.command
def db_reset():
    '''Drop all & create all.'''
    db.drop_all()
    db.create_all()


@manager.command
def create_users():
    '''Create user.'''
    admin = User.create(**{
        "email": "demo@demo.cl",
        "full_name": "Administrador",
        "phone": "555-555-555",
        "avatar": "/static/avatar.png",
        "password": "d3m0.fl4sk",
        "is_admin": True,
        "role": Role.Administrador,
        "language": "es"
    })


@manager.command
def load_data(file="meteo2.csv"):
    '''Cargar datos de meteo.csv'''
    import pandas as pd
    import os
    from datetime import datetime

    data = pd.read_csv(os.path.join(Config.APP_ROOT, file), sep=",")
    # print(data.describe())
    for idx, value in data.iterrows():
        try:
            if not Medicion.query.filter_by(fecha=datetime.strptime(value["Fecha"], "%d-%m-%Y")).first():
                medicion = Medicion()
                medicion.fecha = datetime.strptime(value["Fecha"], "%d-%m-%Y")
                medicion.temperatura = float(value["Temperatura"]) if value["Temperatura"] != '-' else float(data["Temperatura"][idx-1])
                medicion.humedad = float(value["Humedad"]) if value["Humedad"] != '-' else float(data["Humedad"][idx-1])
                medicion.et0 = float(value["et0"]) if value["et0"] != '-' else float(data["et0"][idx-1])
                db.session.add(medicion)
        except Exception as e:
            print("Excepción :", e, " value :", value)
            logging.exception(e)
    db.session.commit()


if __name__ == '__main__':
    manager.run()
