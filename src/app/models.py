# -*- coding: utf-8 -*-
import logging
from app import db, login_manager
from sqlalchemy.orm.attributes import InstrumentedAttribute
from flask_login import UserMixin
from flask import current_app, url_for
from sqlalchemy.schema import UniqueConstraint
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from flask_babel import lazy_gettext as _
import pytz
from abc import abstractmethod
import os


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        db.session.add(obj)
        db.session.commit()
        return obj

    def model_dict(self, **kwargs):
        md = {k: getattr(self, k, None) for (k, v)
              in self.__class__.__dict__.items() if isinstance(v, InstrumentedAttribute)}
        for k, v in kwargs.items():
            md[k] = v
        return md

    def __repr__(self):
        return u"{0}, ins.attrs.:{1}".format(self.__class__, self.model_dict())


class Role:
    Unknown = 0
    Administrador = 1
    Asesor = 2
    Cliente = 3

    @classmethod
    def as_dict(cls):
        return {
            cls.Unknown: "Unknown",
            cls.Administrador: "Administrador",
            cls.Asesor: "Asesor",
            cls.Cliente: "Cliente",
        }

    @classmethod
    def choices(cls):
        return [(key, value) for key, value in cls.as_dict().items()]


class UserStatus:
    Disabled = 0
    Enabled = 1

    @classmethod
    def as_dict(cls):
        return {
            cls.Enabled: _("Enabled"),
            cls.Disabled: _("Disabled"),
        }

    @classmethod
    def choices(cls):
        return [(key, value) for key, value in cls.as_dict().items()]


class User(UserMixin, BaseModel):
    __tablename__ = 'User'
    email = db.Column(db.String(64), nullable=True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)
    role = db.Column(db.Integer, nullable=True, default=Role.Unknown)

    full_name = db.Column(db.String(256), nullable=False)
    phone = db.Column(db.String(64), nullable=True)
    language = db.Column(db.String(10))

    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    last_access = db.Column(db.DateTime(), nullable=True)

    status = db.Column(db.Integer, nullable=True, default=UserStatus.Disabled)
    avatar = db.Column(db.String(256), nullable=True)

    __table_args__ = (
        UniqueConstraint('email', name='uq_user_email'),
    )

    def __repr__(self):
        return u"{0}, id:{1}, email:{2}".format(self.__class__, self.id, self.email)

    @classmethod
    def choices_id_name(cls, query=None):
        if not query:
            query = cls.query
        return [(model.id, model.full_name) for model in query.all()]

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def update_last_access(self):
        self.last_access = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    @classmethod
    def query_by_email(cls, email, query=None):
        if not query:
            query = cls.query
        query = query.filter(cls.email == email)
        return query

    @classmethod
    def by_roles(cls, role_list=None, query=None):
        if not query:
            query = cls.query
        query = query.filter(cls.role.in_(role_list))
        return query


class Priority:
    Baja = 0
    Media = 1
    Alta = 2
    Urgente = 3

    @classmethod
    def as_dict(cls):
        return {
            cls.Baja: ("Baja", "fa-angle-double-down"),
            cls.Media: ("Media", "fa-angle-down"),
            cls.Alta: ("Alta", "fa-angle-up"),
            cls.Urgente: ("Urgente", "fa-angle-double-up"),
        }

    @classmethod
    def choices(cls):
        return [(key, value[0]) for key, value in cls.as_dict().items()]


class FileType:
    File = 'File'
    UserAvatar = 'UserAvatar'
    AttachedFile = 'AttachedFile'

    @classmethod
    def as_dict(cls):
        return {
            cls.File: "File",
            cls.UserAvatar: "UserAvatar",
            cls.AttachedFile: "AttachedFile"
        }

    @classmethod
    def choices(cls):
        return [(key, value) for key, value in cls.as_dict().items()]


class FileStatus:
    ToDo = 0
    Doing = 1
    Done = 2
    Error = 3

    @classmethod
    def as_dict(cls):
        return {
            cls.ToDo: "ToDo",
            cls.Doing: "Doing",
            cls.Done: "Done",
            cls.Error: "Error",
        }

    @classmethod
    def choices(cls):
        return [(key, value) for key, value in cls.as_dict().items()]


class FileModel(BaseModel):
    __tablename__ = 'FileModel'
    filename = db.Column(db.Text, nullable=False)
    filepath = db.Column(db.Text, nullable=False)
    upload_filename = db.Column(db.Text, nullable=True)
    type = db.Column(db.String(64), nullable=False)

    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    modified_at = db.Column(db.DateTime(), default=datetime.utcnow)

    created_by_id = db.Column(db.Integer, db.ForeignKey('User.id', name='fk_File_created_User_id'))
    created_by = db.relationship('User', foreign_keys=[created_by_id],
                                 backref=db.backref('files_created', lazy='dynamic'))

    modified_by_id = db.Column(db.Integer, db.ForeignKey('User.id', name='fk_File_modified_User_id'))
    modified_by = db.relationship('User', foreign_keys=[modified_by_id],
                                  backref=db.backref('files_modified', lazy='dynamic'))

    status = db.Column(db.Integer, nullable=False, default=0, server_default='0')
    task_id = db.Column(db.String(36), nullable=True)
    status_message = db.Column(db.Text, nullable=True)
    published = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, type):
        self.type = type
        self.filepath = FileModel.get_path(self.type)

    @property
    def full_filepath(self):
        return os.path.join(self.filepath, self.filename)

    @abstractmethod
    def generate_filename(self, *args, **kwargs):
        raise NotImplementedError()

    @classmethod
    def allowed_file_extension(cls, filename, type):
        IMG_ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'}
        IMG_TYPES = [FileType.UserAvatar]
        # EXCEL_ALLOWED_EXTENSIONS = {'xls', 'xlsx', 'XLS', 'XLSX'}
        # EXCEL_TYPES = []
        PDF_ALLOWED_EXTENSIONS = {'pdf', 'PDF'}
        PDF_TYPES = [FileType.AttachedFile]

        if type in IMG_TYPES and filename.rsplit('.', 1)[1] in IMG_ALLOWED_EXTENSIONS:
            return '.' in filename and filename.rsplit('.', 1)[1] in IMG_ALLOWED_EXTENSIONS
        # if type in EXCEL_TYPES and filename.rsplit('.', 1)[1] in EXCEL_ALLOWED_EXTENSIONS:
        #     return '.' in filename and filename.rsplit('.', 1)[1] in EXCEL_ALLOWED_EXTENSIONS
        if type in PDF_TYPES and filename.rsplit('.', 1)[1] in PDF_ALLOWED_EXTENSIONS:
            return '.' in filename and filename.rsplit('.', 1)[1] in PDF_ALLOWED_EXTENSIONS

    @classmethod
    def get_path(cls, file_type):
        _UPLOADS_ROOT = current_app.config.get('UPLOADS_ROOT', None)
        _DOCS_ROOT = current_app.config.get('DOCS_ROOT', None)
        if not _UPLOADS_ROOT:
            raise ValueError("Could not find UPLOADS_ROOT in config")
        if not _DOCS_ROOT:
            raise ValueError("Could not find UPLOADS_ROOT in config")

        if file_type == FileType.File:
            return os.path.join(_UPLOADS_ROOT, FileType.File)
        elif file_type == FileType.UserAvatar:
            return os.path.join(_UPLOADS_ROOT, FileType.UserAvatar)
        elif file_type == FileType.AttachedFile:
            return os.path.join(_DOCS_ROOT, FileType.AttachedFile)

        else:
            return None

    def upload_file(self, file_data, **opts):
        from flask_login import current_user
        from werkzeug.utils import secure_filename

        if file_data.filename != '':
            if FileModel.allowed_file_extension(file_data.filename, self.type):
                try:
                    filename = secure_filename(file_data.filename)
                    self.upload_filename = filename
                    self.filename = secure_filename(
                        self.generate_filename(
                            current_user=current_user,
                            upload_filename=self.upload_filename))

                    file_data.save(self.full_filepath)

                    self.modified_by_id = current_user.id
                    self.status = FileStatus.Done
                    return True
                except Exception as e:
                    print(e)
                    self.status = FileStatus.Error
                    return False
            else:
                print('Extension not allowed')
                self.status = FileStatus.Error
                return False
        else:
            print('No filename')
            self.status = FileStatus.Error
            return False

    def create_file(self, func_init_creation, *args, **kwargs):
        self.filename = self.generate_filename(*args, **kwargs)
        kwargs.update({"file_model": self})
        kwargs.update(self.__dict__)
        func_init_creation(*args, **kwargs)


class UserAvatar(FileModel):
    __tablename__ = 'UserAvatar'
    id = db.Column(db.Integer, db.ForeignKey('FileModel.id', name='fk_UserAvatar_FileModel_id'), primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    user = db.relationship('User', backref=db.backref('user_avatar', lazy='dynamic'))

    __mapper_args__ = {
        'polymorphic_identity': FileType.UserAvatar
    }

    def __init__(self, **kwargs):
        super().__init__(FileType.UserAvatar)
        self.user_id = kwargs.get("user_id", None)

    def generate_filename(self, *args, **kwargs):
        user_id = kwargs.get('user_id', self.user_id)
        if not user_id:
            user_id = 0
        filename = "{user_id}".format(user_id=user_id)
        return filename


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page,
                                **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page,
                                **kwargs) if resources.has_prev else None
            }
        }
        return data


class Medicion(PaginatedAPIMixin, BaseModel):
    __tablename__ = 'Medicion'
    temperatura = db.Column(db.Float, nullable=True)
    humedad = db.Column(db.Float, nullable=True)
    et0 = db.Column(db.Float, nullable=True)
    fecha = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    def to_dict(self):
        data = {
            'id': self.id,
            'temperatura': self.temperatura,
            'humedad': self.humedad,
            'et0': self.et0,
            'fecha': self.fecha.isoformat() + 'Z'
        }
        return data

    def from_dict(self, data):
        for field in ['temperatura', 'humedad', 'et0']:
            if field in data:
                setattr(self, field, data[field])

    def save(self):
        db.session.add(self)
        db.session.commit()

    def as_dict(self):
        data = {}
        for attr, value in self.__class__.__dict__.items():
            data[attr] = value
            return data


def utc2local(utc_datetime, timezone_name='America/Santiago'):
    try:
        local_tz = pytz.timezone(timezone_name)
    except Exception as e:
        local_tz = pytz.timezone('America/Santiago')

    if utc_datetime.tzinfo is None:
        utc_datetime = utc_datetime.replace(tzinfo=pytz.UTC)

    local_datetime = utc_datetime.astimezone(local_tz)
    return local_datetime


def local2utc(local_datetime, timezone_name='America/Santiago'):
    try:
        local_tz = pytz.timezone(timezone_name)
    except Exception as e:
        local_tz = pytz.timezone('America/Santiago')

    if local_datetime.tzinfo is None:
        local_datetime = local_tz.localize(local_datetime, is_dst=None)
        # local_datetime = local_datetime.replace(tzinfo=local_tz)
    utc_datetime = local_datetime.astimezone(pytz.UTC)
    return utc_datetime
