from app.api import api
from flask import jsonify, request, url_for
from app.models import Medicion
from app import db
from app.api.errors import bad_request


@api.route('/mediciones/<int:id>', methods=['GET'])
def get_medicion(id):
    return jsonify(Medicion.query.get_or_404(id).to_dict())


@api.route('/mediciones', methods=['GET'])
def get_mediciones():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Medicion.to_collection_dict(Medicion.query, page, per_page, 'api.get_mediciones')
    return jsonify(data)


@api.route('/mediciones', methods=['POST'])
def create_medicion():
    data = request.get_json() or {}

    if 'fecha' in data and Medicion.query.filter_by(fecha=data['fecha']).first():
    # if Medicion.query.filter_by(fecha=data['fecha']).first():
        return bad_request("Ya existe medición para esa fecha")
    medicion = Medicion()
    medicion.from_dict(data)
    medicion.save()
    response = jsonify(medicion.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_medicion', id=medicion.id)
    return response


@api.route('/mediciones/<int:id>', methods=['PUT'])
def update_medicion(id):
    medicion = Medicion.query.get_or_404(id)
    data = request.get_json() or {}

    if 'fecha' in data and data['fecha'] != medicion.fecha and \
            Medicion.query.filter_by(fecha=data['fecha']).first():
        return bad_request("Utilizar otra fecha para la medición")
    medicion.from_dict(data)
    medicion.save()
    return jsonify(medicion.to_dict())
