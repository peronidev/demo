from flask import Flask, request, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_babel import Babel
from flask_principal import Principal


db = SQLAlchemy()
bootstrap = Bootstrap()
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
babel = Babel()
principal = Principal()


def create_app(config_name):
    from config import config
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    if not app.config['DEBUG'] and not app.config['TESTING']:
        # configure logging for production
        import logging
        from logging.handlers import SysLogHandler
        handler = SysLogHandler()
        handler.setLevel(logging.INFO)
        app.logger.addHandler(handler)
    else:
        import logging
        from logging.handlers import RotatingFileHandler
        import os
        handler = RotatingFileHandler(
            os.path.join(app.config['LOG_ROOT'], '{0}.log'.format(__name__)),
            maxBytes=10000,
            backupCount=1)
        handler.setLevel(logging.INFO)
        app.logger.addHandler(handler)

    db.init_app(app)
    login_manager.init_app(app)
    bootstrap.init_app(app)
    babel.init_app(app)
    principal.init_app(app)

    from app.demo import demo as demo_bp
    app.register_blueprint(demo_bp)

    from app.auth import auth as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.api import api as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')


    @babel.localeselector
    def get_locale():
        return session.get('language', request.accept_languages.best_match(app.config['LANGUAGES'].keys()))

    @app.route('/')
    def index():
        return redirect(url_for('demo.index'))

    return app
