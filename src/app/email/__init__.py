# -*- coding: utf-8 -*-
import logging
import smtplib
from smtplib import SMTPException
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.header import Header
from email.utils import formataddr


def send_mail(type, **kwargs):
    sender = 'test@test.cl'

    if type == 'recovery':
        user = kwargs.get('user', None)
        password = kwargs.get('password', None)
        receivers = [user.email]

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Recuperación contraseña', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Hola """ + user.full_name + u""",<br>
            <br>
            Bienvenido<br>
            <br>
            <b>Recuperar contraseña</b><br>
            Tu nombre de usuario: """ + user.email + u"""<br>
            Tu contraseña temporal: """ + password + """<br>
            Url de acceso: www.demo.cl<br>
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)

    if type == 'notification':
        user = kwargs.get('user', None)
        order = kwargs.get('order', None)
        receivers = [user.email]

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Nueva solicitud', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Hola """ + user.full_name + u""",<br>
            <br>
            Ha llegado una nueva solicitud<br>
            <br>
            <b>Solicitud</b><br>
            Número solicitud: """ + str(order.folio) + u"""<br>
            Solicitante: """ + order.created_by.full_name + """<br>
            Fecha solicitud: """ + order.local_created_at.strftime('%Y-%m-%d %H:%M:%S') if order.local_created_at else "" + """<br>
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)

    if type == 'new_user':
        user = kwargs.get('user', None)
        password = kwargs.get('password', None)
        receivers = [user.email]

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Nuevo usuario', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Estimado/a """ + user.full_name + u""",<br>
            <br>
            Se ha creado una cuenta de usuario para demo<br>
            <br>
            <b></b><br>
            Nombre de usuario: """ + user.email + u"""<br>
            Contraseña temporal: """ + password + """<br>
            Url de acceso: www.demo.cl<br>
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)

    if type == 'password_edited':
        user = kwargs.get('user', None)
        password = kwargs.get('password', None)
        receivers = [user.email]

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Contraseña editada', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Estimado/a """ + user.full_name + u""",<br>
            <br>
            Se ha editado la contraseña de su cuenta en demo<br>
            <br>
            <b></b><br>
            Nombre de usuario: """ + user.email + u"""<br>
            Contraseña temporal: """ + password + """<br>
            Url de acceso: www.demo.cl<br>
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)

    if type == 'disabled_user':
        user = kwargs.get('user', None)
        receivers = [user.email]

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Cuenta desactivada', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Estimado/a """ + user.full_name + u""",<br>
            <br>
            Se ha desactivado su cuenta de usuario para demo<br>
            <br>
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)

    if type == 'invitation':
        correos = kwargs.get('correos_list', None)
        receivers = correos

        message = MIMEMultipart('related')
        message_alt = MIMEMultipart('alternative')
        message_alt.set_charset('utf8')
        message.attach(message_alt)

        message['Subject'] = Header(u'Invitación a demo', 'UTF-8')

        body = """<html>
            <head></head>
            <body>
            <p>Estimado/a,<br>
            <br>
            Se le ha invitado a demo.<br>
            <br>
            Por favor, ingrese a www.demo.cl
            <br>
            </p>
            </body>
            </html>"""

        send(body, message, message_alt, sender, receivers)


def send(body, message, message_alt, sender, receivers):
    try:
        body = body.encode('UTF-8')
    except Exception as e:
        logging.exception(e)

    message['To'] = ', '.join(receivers)
    message['From'] = formataddr((str(Header('Equipo soporte', 'utf-8')), sender))
    message_alt.attach(MIMEText(body, 'html', _charset='UTF-8'))

    try:
        smtp_obj = smtplib.SMTP('smtp.gmail.com', 587)
        smtp_obj.starttls()
        smtp_obj.login(sender, 'Password')
        smtp_obj.sendmail(sender, receivers, message.as_string().encode('UTF-8'))
        smtp_obj.quit()
    except SMTPException as er:
        logging.exception(er)
        print(er)
    # except Exception as e:
    #     logging.exception(e)


def random_password():
    from random import choice
    length = 10
    values = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=@#%&+"
    password = ''.join([choice(values) for i in range(length)])
    return password
