from flask_wtf import FlaskForm as Form
from flask_wtf.file import FileField, FileRequired
from flask_babel import lazy_gettext as _
from wtforms import StringField, IntegerField, TextAreaField, SubmitField, \
    SelectField, DateTimeField, DateField, DecimalField, BooleanField, SelectMultipleField, PasswordField, RadioField, \
    FileField, HiddenField
from wtforms.validators import DataRequired, Length, Email, Optional


def copy_from_model(form, model, attr_list):
    for attr in attr_list:
        setattr(getattr(form, attr), 'data', getattr(model, attr))


def copy_to_model(form, model, attr_list):
    for attr in attr_list:
        setattr(model, attr, getattr(getattr(form, attr), 'data'))

Form.copy_from_model = copy_from_model
Form.copy_to_model = copy_to_model


class UserForm(Form):
    email = StringField(_('Email'), validators=[Optional(), Email("Not valid email")])
    full_name = StringField(_('Fullname'), validators=[Length(1, 256)])
    # phone = StringField(_('Phone'), validators=[Length(1, 64)])
    # position = StringField(_('Position'), validators=[Length(0, 64)])
    # avatar = FileField(_('Image file (*.jpg, *.png)'))
    role = SelectField(_('Role'), coerce=int)
    # active = BooleanField(_('Active'))

    # country_id = SelectField(_('Country'), coerce=int)
    national_id = StringField(_('National ID'))
    # language = SelectField(_('Language'), coerce=str)
    # client_id = SelectField(_('Client'), coerce=int)
    random_password = BooleanField(_('Random password'))

    password = PasswordField(_('Password'))
    # status = SelectField(_('Status'), coerce=int)

    submit = SubmitField(_('Ingresar'))

    def from_model(self, model):
        copy_from_model(self, model, ['email', 'full_name', 'role', 'national_id'])

    def to_model(self, model):
        copy_to_model(self, model, ['email', 'full_name', 'role', 'national_id'])

        if self.password.data:
            model.password = self.password.data


class DatesForm(Form):
    inicio = DateTimeField(_(u'Fecha inicio'))
    termino = DateTimeField(_(u'Fecha término'))
    submit = SubmitField(_('Filtrar'))
