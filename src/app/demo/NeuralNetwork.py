import numpy as np
from config import Config
import os
# from sklearn.metrics import mean_squared_error


def sigmoid(x):
    return 1.0/(1 + np.exp(-x))


def sigmoid_derivative(x):
    return x * (1.0 - x)


class NeuralNetwork:
    def __init__(self):
        self.input = np.array([])
        self.weights1 = np.load(os.path.join(Config.DEMO_ROOT, "weights1.npy"))
        self.weights2 = np.load(os.path.join(Config.DEMO_ROOT, "weights2.npy"))
        self.output = np.array([])

    def feedforward(self):
        self.layer1 = sigmoid(np.dot(self.input, self.weights1))
        self.output = sigmoid(np.dot(self.layer1, self.weights2))
