# -*- coding: utf-8 -*-
from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Length
from flask_babel import lazy_gettext as _


class LoginForm(Form):
    email = StringField(_(u'Email'), validators=[DataRequired(), Length(1, 64)])
    password = PasswordField(_(u'Password'), validators=[DataRequired()])
    remember_me = BooleanField(_(u'Keep connected'))
    submit = SubmitField(_(u'OK'))


class ForgotPasswordForm(Form):
    email = StringField(_(u'Email'), validators=[DataRequired(), Length(1, 64)])
    submit = SubmitField(_(u'Send recovery email'))

