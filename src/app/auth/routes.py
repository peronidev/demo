# -*- coding: utf-8 -*-
from flask import render_template, current_app, request, redirect, url_for, \
    flash, session
from flask_login import login_user, logout_user, login_required, current_user
from app.models import User
from app.auth import auth
from app import email
from app import db
from app.auth.forms import LoginForm, ForgotPasswordForm
from flask_principal import identity_changed, Identity, AnonymousIdentity, \
    RoleNeed, UserNeed, identity_loaded, Need
from flask_babel import gettext


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('demo.index'))
    else:
        form = LoginForm()
        session.pop('language', None)
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()
            if user is None or not user.verify_password(form.password.data):
                flash(gettext(u"Invalid email or password"))
                return redirect(url_for('auth.login'))

            login_user(user, form.remember_me.data)

            identity_changed.send(current_app._get_current_object(),
                                  identity=Identity(user.id))
            # if current_user.last_access is None:
            #     current_user.update_last_access()
            #     return redirect(url_for('tracking.change_password'))
            # current_user.update_last_access()
            # session['language'] = user.language
            return redirect(request.args.get('next') or url_for('demo.index'))
        return render_template('login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()

    for key in ('identity.name', 'identity.auth_type', 'faena_id', 'faena_name'):
        session.pop(key, None)

    identity_changed.send(current_app._get_current_object(),
                          identity=AnonymousIdentity())

    flash(gettext(u"You have logged out"))
    return redirect(url_for('demo.index'))


@identity_loaded.connect
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))
        user = User.query.filter_by(id=current_user.id).first()
        identity.provides.add(RoleNeed(user.role))


@auth.route('/forgot_password', methods=['GET', 'POST'])
def forgot_password():
    form = ForgotPasswordForm()
    if request.method == 'GET':
        return render_template('forgot_password.html', form=form)
    elif request.method == 'POST':
        user = User.query.filter(User.email == form.email.data).first()
        if user is not None:
            password = email.random_password()
            user.password = password
            db.session.add(user)
            db.session.commit()
            email.send_mail('recovery', user=user, password=password)
            flash(gettext('A temporary password has been sent to your email'))
        else:
            flash(gettext('There is no user with this email'))
        return redirect(url_for('auth.login'))
